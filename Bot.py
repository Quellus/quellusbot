#Imports
import twitchio
from twitchio.ext import commands
import commandParser
import Rewards

class Bot(commands.Bot): # This is the main config for the bot
    def __init__(self, irc_token, client_id, nick, init_channel):
        super().__init__(irc_token=irc_token, client_id=client_id, nick=nick, prefix='!',
                         initial_channels=[init_channel])
        commandParser.readAndParseCommands()

    
    async def event_ready(self):
        """Prints a message to console to show the bot is working"""
        print(f'Ready | {self.nick}')
        
    async def event_message(self, message):
        """Handles reading messages and commands"""
        if message.author.name.lower() == self.nick.lower(): # ignore messages from the bot
            return

        response = commandParser.getMessage(message.content)

        if "custom-reward-id" in message.tags:
            Rewards.handleReward(message.tags["custom-reward-id"], message.content, message.author.name);
            return

        if not response:
            print("message is not a command")
            return

        channel = self.get_channel(self.initial_channels[0])
        await channel.send(response)
        print("sent:", response)

