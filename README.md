# QuellusBot

**This is a twitch chatbot coded in python for the streamer Quellus who can be found here https://www.twitch.tv/quellus**

Developed by: **TypoCode**, **Quellus**


### Packages

* [TwitchIO](https://github.com/TwitchIO/TwitchIO)
* threading

### Running the bot
Create a file called "config.py" with the following information:
```
irc_token=<oauth token for the bot>
client_id=<client id for the bot program>
nick=<name of bot channel>
initial_channel=<name of stream channel>
```

