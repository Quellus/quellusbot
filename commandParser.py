import math # used for math.floor()
import random

commands = [] # a sorted list where each entry is a tuple containing the command and a tuple of words in the message

def readAndParseCommands():
    """ Reads and parses "commands.txt" and populates the commands list"""
    comsFile = None
    try:
        comsFile = open("commands.txt")
    except FileNotFoundError as e:
        print("Error: commands file not found")
        return None

    line = comsFile.readline()
    while line != "":
        lineSplit = line.split()
        insertSorted(lineSplit[0], lineSplit[1:])
        line = comsFile.readline()
       
    comsFile.close()

def insertSorted(cmdStr: str, response: list):
    """Inserts a command into the commands list in a sorted position"""
    if len(commands) == 0:
        commands.append((cmdStr, response))

    else:
        for i in range(len(commands)):
            if commands[i][0] >= cmdStr:
                commands.insert(i, (cmdStr, response))
                return

        commands.append((cmdStr, response))
    
def getMessage(message: str):
    """Returns the message associated with a command or returns None if command does not exist"""
    messageWords = message.split()
    if len(messageWords) > 0:
        responseTuple = binarySearch(messageWords[0])
        if responseTuple == None:
            return None
        responseTuple = handleResponseCodes(responseTuple)
        responseString = " ".join(responseTuple)
        return responseString

def handleResponseCodes(response: list):
    """Checks the response for codes and returns the response with the code replaced"""
    newResponse = []
    for n, word in enumerate(response):
        newResponse.append(word)
        if word.startswith("#@$") and word.startswith("rf", 3):
            code = word.split("=")
            with open(code[1], "r") as readfile:
                fileContents = readfile.readlines()
                newResponse[n] = random.choice(fileContents)
    return newResponse
                

def binarySearch(command: str):
    """Binary search algorithm which returns the index of the command or None if command does not exist"""
    low = 0
    high = len(commands) - 1

    while high >= low:
  
        mid = low + math.floor((high - low) / 2)
  
        if commands[mid][0] == command: 
            return commands[mid][1] 
          
        elif commands[mid][0] > command: 
            high = mid - 1
  
        else: 
            low = mid + 1

    return None

