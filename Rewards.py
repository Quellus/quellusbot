import os
import git

writeBookId = "c2b556e9-2114-4a1a-a8c7-ddacc9d0007c";

def handleReward(rewardType: str, message: str, username: str):
    if rewardType == writeBookId:
        writeBookReward(message, username)

def writeBookReward(message: str, username: str):
        origDir = os.getcwd() # Remember which directory we were in
        os.chdir("twitchchatbook")

        repo = git.Repo("")
        origin = repo.remoteorigin = repo.remote(name='origin')
        origin.pull()

        with open("README.md", "a") as book:
            book.write(message + " ")

        repo.index.add(["README.md"])
        repo.index.commit("Adding sentence written by " + username)
        origin.push()

        os.chdir(origDir)

