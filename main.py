#!/usr/bin/env python3

# QuellusBot
# A bot made in twitchio

#Imports
from Bot import Bot

def print_config_error():
  """Logs an error and exits the program"""
  print("Config file error")
  print("Either a file named \"config.txt\" does not exist or it is not in the proper format")
  print("Please create it and format it accordingly:\n")
  print("irc_token=<bot channel oauth>\nclient_id=<the id for twitchbot client>\nnick=<bot channel name>\ninitial_channel=<streaming channel name>")
  exit(0)

def parse_config(config_lines: list):
  """Parses the array of lines read from the config file and returns a dictionary of the data"""
  config = {}

  if len(config_lines) < 4:
    print_config_error()

  for line in config_lines:
    words = line.split("=")
    if len(words) >= 2:
      config[words[0]] = words[1].strip(" ")

  if "irc_token" in config.keys() \
      and "client_id" in config.keys() \
      and "nick" in config.keys() \
      and "initial_channel" in config.keys():
    return config
  else:
    print_config_error()

def main():
  """Parses the config file and starts the bot"""
  try:
    file = open("config.txt")
    input = file.read()
    file.close()
  except FileNotFoundError as e:
    print_config_error()

  config_lines = input.split("\n")
  config = parse_config(config_lines)

  irc_token = config["irc_token"]
  client_id = config["client_id"]
  nick = config["nick"]
  init_channel = config["initial_channel"]

  bot = Bot(irc_token, client_id, nick, init_channel)
  bot.run()

main()

